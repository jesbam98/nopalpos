from django.contrib import admin
from .models import Unit, Item
# Register your models here.

admin.site.register(Unit)
admin.site.register(Item)