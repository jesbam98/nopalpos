from django.forms import ModelForm, TextInput, Textarea
from .models import Item


class ItemForm(ModelForm):
    class Meta:
        model = Item
        fields = '__all__'
        widgets = {
            'description': Textarea(attrs={'class': 'textarea', 'rows': '2'})
        }
