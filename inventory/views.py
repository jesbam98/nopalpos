from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Item, Unit
from .forms import ItemForm

# Create your views here.

# Unit Views


class UnitListView(ListView):
    model = Unit


class UnitCreateView(CreateView):
    model = Unit
    fields = '__all__'
    success_url = reverse_lazy('inventory:unit-list')


class UnitUpdateView(UpdateView):
    model = Unit
    fields = '__all__'
    template_name = 'inventory/unit_edit_form.html'
    success_url = reverse_lazy('inventory:unit-list')


class UnitDeleteView(DeleteView):
    model = Unit
    success_url = reverse_lazy('inventory:unit-list')

# Item Views


class ItemListView(ListView):
    model = Item


class ItemDetailView(DetailView):
    model = Item


class ItemCreateView(CreateView):
    form_class = ItemForm
    template_name = 'inventory/item_form.html'
    success_url = reverse_lazy('inventory:item-list')


class ItemUpdateView(UpdateView):
    model = Item
    template_name = 'inventory/item_edit_form.html'
    form_class = ItemForm
    success_url = reverse_lazy('inventory:item-list')


class ItemDeleteView(DeleteView):
    model = Item
    success_url = reverse_lazy('inventory:item-list')
