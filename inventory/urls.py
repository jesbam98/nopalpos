from django.urls import path
from . import views


app_name = 'inventory'
urlpatterns = [
    path('items/', views.ItemListView.as_view(), name='item-list'),
    path('items/create/', views.ItemCreateView.as_view(), name='item-create'),
    path('items/<int:pk>/', views.ItemDetailView.as_view(), name='item-detail'),
    path('items/<int:pk>/update/', views.ItemUpdateView.as_view(), name='item-update'),
    path('items/<int:pk>/delete/', views.ItemDeleteView.as_view(), name='item-delete'),
    path('units/', views.UnitListView.as_view(), name='unit-list'),
    path('units/create/', views.UnitCreateView.as_view(), name='unit-create'),
    path('units/<int:pk>/update/', views.UnitUpdateView.as_view(), name='unit-update'),
    path('units/<int:pk>/delete/', views.UnitDeleteView.as_view(), name='unit-delete'),
]
