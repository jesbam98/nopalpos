from django import template

register = template.Library()


@register.filter
def addcss(field, arg):
    return field.as_widget(attrs={"class": arg})
