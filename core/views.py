from django.shortcuts import render
from django.views.generic import TemplateView
from inventory.models import Item
# Create your views here.


class DashboardView(TemplateView):
    template_name = 'core/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context['items'] = Item.objects.count()
        return context